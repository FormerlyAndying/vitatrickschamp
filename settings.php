<?php
// Specify EMAIL (FROM) Address:
$MAIL_SERVER_FROM = "burandbygaming@gmail.com";
// Specify Site URL to be shown in messages
$SITE_NAME = "http://vitatricks.vercel.app";
// Specify message to be sent with every email
$MAIL_BODY = 'This message is intended for use with a PlayStation Vita console<br><br>Using a PlayStation Vita click the attached file and then click the WWW Browser icon.<br><br><i>This email was sent using '.$SITE_NAME.'</i>';
?>
